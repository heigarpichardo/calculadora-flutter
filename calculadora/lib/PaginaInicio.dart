
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

final txtEntrada = TextEditingController();
final txtResultado = TextEditingController();

class _MyHomePageState extends State<MyHomePage> {

void blanquear() {
    txtEntrada.text = "";
    txtResultado.text = "";
  }
  
  void calculos(String letra) {

    if (letra.trim() == "AC")
    {
      blanquear();
      setState(() {});
      return;
    }

    if (letra.trim() == "=" && txtEntrada.text.isEmpty)
    {
      txtEntrada.text = "";
      return;
    }

    if ((letra.trim() == "+" || letra.trim() == "-" || letra.trim() == "/" || letra.trim() == "*") 
    && txtResultado.text.isNotEmpty && txtEntrada.text.isEmpty)
    {
      txtEntrada.text += txtResultado.text.trim() + letra.trim();
      return;
    }

    if (letra.trim() == "C")
    {
      txtEntrada.text = "";
      setState(() {});
      return;
    }
    
    if (letra.trim() == "=" && txtEntrada.text.isNotEmpty)
    {
      Parser p = Parser();
      Expression exp = p.parse(txtEntrada.text);
      ContextModel cm = ContextModel();

      double eval = exp.evaluate(EvaluationType.REAL, cm);

      /*if(txtResultado.text.isNotEmpty)
      {
        txtResultado.text += "\n";
      }*/

      txtResultado.text = eval.toString();
      txtEntrada.text = "";

      setState(() {});
      return;
    }

    txtEntrada.text += letra;


    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[300],/*,
      appBar: 
        AppBar(
          title: Text(widget.title),
        ),*/
      body: 
      Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
            child: 
            new TextField(
              decoration: new InputDecoration.collapsed(
                  hintText: "",
                  hintStyle: TextStyle(
                    fontSize: 28,
                    fontFamily: 'RobotoMono',
                  )),
              style: TextStyle(
                fontSize: 28,
                fontFamily: 'RobotoMono',
              ),
              textAlign: TextAlign.right,
              controller: txtResultado,
              onTap: () =>
                  FocusScope.of(context).requestFocus(new FocusNode()),
            )
          ),
          new Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: 
            TextField(
              decoration: new InputDecoration.collapsed(
                  hintText: "0",
                  fillColor: Colors.deepPurpleAccent,
                  hintStyle: TextStyle(fontFamily: 'RobotoMono')),
              textInputAction: TextInputAction.none,
              keyboardType: TextInputType.number,
              style: TextStyle(
                  fontSize: 30,
                  fontFamily: 'RobotoMono',
                  fontWeight: FontWeight.bold
                  // color: Colors.deepPurpleAccent
              ),
              textAlign: TextAlign.right,
              controller: txtEntrada,
            )
          ),
          Container(
            margin: const EdgeInsets.only(top: 0),
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              //color: Colors.teal[100],
              borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
              gradient: 
              LinearGradient(
                begin: Alignment.center,
                end: Alignment.bottomCenter, // 10% of the width, so there are ten blinds.
                colors: [Colors.yellow[100], Colors.yellow[50]], // whitish to gray
                tileMode: TileMode.clamp, // repeats the gradient over the canvas 
              )
            ),
            child: Row(
              children: [ 
                Column(
                  children: [
                    botonC("AC",Colors.yellow[400]),
                    boton("1",Colors.transparent),
                    boton("4",Colors.transparent),
                    boton("7",Colors.transparent),
                    boton("0",Colors.transparent)
                  ],
                ),
                Column(
                  children: [
                    botonC("C",Colors.yellow[400]),
                    boton("2",Colors.transparent),
                    boton("5",Colors.transparent),
                    boton("8",Colors.transparent),
                    boton("",Colors.transparent)
                  ],
                ),
                Column(
                  children: [
                    botonC("%",Colors.yellow[400]),
                    boton("3",Colors.transparent),
                    boton("6",Colors.transparent),
                    boton("9",Colors.transparent),
                    boton(".",Colors.transparent)
                  ],
                ),
                Column(
                  children: [
                    botonC("/",Colors.yellow[400]),
                    botonC("*",Colors.yellow[400]),
                    botonC("-",Colors.yellow[400]),
                    botonC("+",Colors.yellow[400]),
                    botonC("=",Colors.black12)
                  ],
                ),
              ]
            )
          )
        ] 
      )
    );
  }
  Widget boton(btntxt, Color btnColor) {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: FlatButton(
        child: Text(
          btntxt,
          style: TextStyle(
              fontSize: 28.0, color: Colors.teal[400], fontFamily: 'RobotoMono'),
        ),
        onPressed: () {
          calculos(btntxt);
        },
        color: btnColor,
        padding: EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: CircleBorder(),
      ),
    );
  }

  Widget botonC(btntxt, Color btnColor) {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: FlatButton(
        child: Text(
          btntxt,
          style: TextStyle(
              fontSize: 28.0, color: Colors.teal[400], fontFamily: 'RobotoMono'),
        ),
        onPressed: () {
          calculos(btntxt);
        },
        color: btnColor,
        padding: EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      ),
    );
  }
}
